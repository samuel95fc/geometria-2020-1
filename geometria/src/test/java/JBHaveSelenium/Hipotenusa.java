package JBHaveSelenium;

import static org.junit.Assert.*;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Hipotenusa {

	WebDriver driver;

	@Test
	public void geometriaChromeDriver() {
		System.setProperty("webdriver.chrome.driver",
				"C:/Users/Samuel Jesus/OneDrive/Documentos/BES/BANCO DE DADOS/driver/chromedriver_win32/chromedriver.exe");
	}

	@Test
	@Given("funcionalidade calculo triangulos")
	public void funcaoCalcular() {
		driver = new ChromeDriver();
		driver.get("C:/Users/Samuel Jesus/git/geometria-2020-1/geometria/src/main/webapp/triangulo.html");
	}
	
	@Test
	@When("selecionar tipo calculo hipotenusa")
	public void calcularHipotenusa() {
		Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
		select.selectByVisibleText("Hipotenusa");
	}
	
	@Test
	@When("informar 3 para $cateto1")
	public void valorCateto1() {
		WebElement inputValorCateto1 = driver.findElement(By.id("cateto1"));
		inputValorCateto1.sendKeys("3");
	}
	
	@Test
	@When("informar 4 para $cateto2")
	public void valorCateto2() {
		WebElement inputValorCateto2 = driver.findElement(By.id("cateto2"));
		inputValorCateto2.sendKeys("4");
	}
	
	@Test
	@When("solicitar calculo realizado")
	public void calcular() {
		WebElement buttonCalcular = driver.findElement(By.id("calcularBtn"));
		buttonCalcular.click();
	}

	@Test
	@Then("hipotenusa calculada sera 5")
	public void resultado() {
		WebElement output = driver.findElement(By.id("hipotenusa"));
		int valorEsperado = 5;
		assertEquals(valorEsperado, output);
	}
}
