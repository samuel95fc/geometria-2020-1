package JBHaveSelenium;

import static org.junit.Assert.*;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Cateto {

	WebDriver driver;

	@Test
	public void geometriaChromeDriver() {
		System.setProperty("webdriver.chrome.driver\",\r\n" + 
				"				\"C:/Users/Samuel Jesus/OneDrive/Documentos/BES/BANCO DE DADOS/driver/chromedriver_win32/chromedriver.exe");
	}

	@Test
	@Given("funcionalidade calculo triangulos")
	public void funcaoCalcular() {
		driver = new ChromeDriver();
		driver.get("C:/Users/Samuel Jesus/git/geometria-2020-1/geometria/src/main/webapp/triangulo.html");
	}

	@Test
	@When("selecionar tipo calculo cateto")
	public void calcularHipotenusa() {
		Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
		select.selectByVisibleText("Cateto");
	}
	
	@Test
	@When("informar 6 para cateto1")
	public void valorCateto1() {
		WebElement inputValorCateto1 = driver.findElement(By.id("cateto1"));
		inputValorCateto1.sendKeys("6");
	}
	
	@Test
	@When("informar 10 para hipotenusa")
	public void valorCateto2() {
		WebElement inputValorHipotenusa = driver.findElement(By.id("hipotenusa"));
		inputValorHipotenusa.sendKeys("10");
	}
	
	@Test
	@When("solicitar calculo realizado")
	public void calcular() {
		WebElement buttonCalcular = driver.findElement(By.id("calcularBtn"));
		buttonCalcular.click();
	}

	@Test
	@Then("cateto2 calculado sera 8")
	public void resultado() {
		WebElement output = driver.findElement(By.id("cateto2"));
		int valorEsperado = 8;
		assertEquals(valorEsperado, output);
	}
}
